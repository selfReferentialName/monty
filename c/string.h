// interface strings with Oxyl

#ifndef OXYL_STRING_H
#define OXYL_STRING_H

#include <stdint.h>

struct ox_string {
	int32_t bound; // useless by C except as bound checking since the actual value is null terminated for use with C
	char* s;
};

#endif
