// interface arrays with Oxyl

#ifndef OXYL_ARRAY_H
#define OXYL_ARRAY_H

#include <stdint.h>

struct ox_array {
	int32_t bound; // useless by C except as bound checking since the actual value is null terminated for use with C
	void* s;
};

#endif
