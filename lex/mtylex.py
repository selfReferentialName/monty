#!/usr/bin/python

# lexer for bootstrapping oxyl off of python

import lex.lex as lex

# debug stuff and flags

verbose = False

# token names

keywords_lex = (
    'IMPORT',
    'CLASS',
    'BUILTIN',
)

types = (
    'INT_T',
    'FLOAT_T',
    'STRING_T',
    'ARRAY_T',
    'VOID_T',
    'WSTRING_T',
    'VEC_T',
    'MAT_T',
)

descriptors = (
    'GLOBAL_D', # forces external linkage
    'PUBLIC_D', # disables all name mangling, forces global and ccc
    'COLD_D', # forces coldcc
    'COMMON_D', # not actually common, but linkonce_odr. Basically common with less restrictions
)

# literals is already taken, therefore, they are lit
lits = (
    'INT',
    'STRING',
    'WSTRING',
    'FLOAT',
)

operators = (
    'COMMA',
    'EQUALS',
    'PLUS',
    'TIMES',
    'DIV',
    'REM',
    'AND',
    'OR',
    'XOR',
    'NOT',
    'COLON'
)

opgroups = (
    'EQUALS',
    'MULDIV',
    'ADDSUB',
    'CMP',
    'LOGIC',
)

t_MULDIV = r'[*/]'
t_ADDSUB = r'[+-]'
t_CMP = r'==|<=|>='
t_LOGIC = r'[&|^:]'

groupers = (
    'BRACKET_L',
    'BRACKET_R',
    'PAREN_L',
    'PAREN_R',
)

misc = (
    'IDENTIFIER',
    'LESS',
    'GREATER',
    'COMMENT_L',
    'COMMENT_ML',
    'NEWLINE',
    'TAB',
    'DOT',
    'MINUS',
)

tokens = keywords_lex + types + ('DESC_D',) + lits + opgroups + groupers + misc

# token rules

t_ignore = ' '

t_IMPORT = r'import'
t_INT_T = r'int'
t_STRING_T = r'string'
t_ARRAY_T = r'array'
t_VOID_T = r'void'
#t_COMMA = r'\,'
t_DOT = r'\.'
t_EQUALS = r'='
t_BRACKET_L = r'\['
t_BRACKET_R = r'\]'
t_LESS = r'\<'
t_GREATER = r'>'
t_TAB = r'\t'
r_GLOBAL_D = r'global'
r_PUBLIC_D = r'public'
r_COLD_D = r'cold'
r_COMMON_D = r'common'
t_DESC_D = r'global'
t_CLASS = r'class'
t_WSTRING_T = r'wstring'
t_VEC_T = r'vec'
t_MAT_T = r'mat'
#t_PLUS = r'+'
#t_MINUS = r'-'
#t_TIMES = r'*'
#t_DIV = r'/'
#t_REM = r'%'
#t_AND = r'&'
#t_OR = r'|'
#t_XOR = r'^'
#t_NOT = r'!'
#t_COLON = r':'
t_PAREN_L = r'\('
t_PAREN_R = r'\)'
#t_OPERATOR = r'[,=+-*/%&|^!:]'
t_FLOAT_T = r'float'

# keywords that look like identifiers

keywords = [
    ('IMPORT', t_IMPORT),
    ('INT_T', t_INT_T),
    ('STRING_T', t_STRING_T),
    ('ARRAY_T', t_ARRAY_T),
    ('VOID_T', t_VOID_T),
    ('DESC_D', r_GLOBAL_D),
    ('DESC_D', r_PUBLIC_D),
    ('DESC_D', r_COLD_D),
    ('DESC_D', r_COMMON_D),
    ('CLASS', t_CLASS),
    ('BUILTIN', 'new'),
    ('BUILTIN', 'local'),
    ('BUILTIN', 'use'),
    ('BUILTIN', 'free'),
    ('WSTRING_T', t_WSTRING_T),
    ('VEC_T', t_VEC_T),
    ('MAT_T', t_MAT_T),
    ('FLOAT_T', t_FLOAT_T),
]

def t_IDENTIFIER(t):
    r'[A-Za-z][A-Za-z0-9]*'
    for ty, re in keywords:
        if t.value == re:
            t.type = ty
    return t

def t_INT(t):
    r'\d+'
    t.value = int(t.value)
    return t

def t_FLOAT(t):
    r'\d+.\d*'
    t.value = float(t.value)
    return t

def t_STRING(t):
    r'"([^"]|\")*"'
    t.value = t.value[1:-1]
    t.value = t.value.replace('\\"', '"')
    t.value = t.value.replace('\\\\', '\\')
    return t

def t_WSTRING(t):
    '\'w\'([^\']|\\\')*\''
    t.value = t.value[1:-1]
    t.value = t.value.replace('\\\'', '\'')
    t.value = t.value.replace('\\\\', '\\')
    return t

def t_COMMENT_L(t):
    r'\#[^\n]*'
#   t.lexer.lineno += 1
    return None

def t_COMMENT_ML(t):
    r'\#\#([^\n]*\n\#?[^\#])+\#\#'
    t.lexer.lineno += t.value.count('\n')
    return None

def t_NEWLINE(t):
    r'\n+'
    t.lexer.lineno += len(t.value)
    return t

def t_error(t):
    print("Illegal token: " + str(t))

# initialize

lexer = lex.lex()

def inits(s):
    global lexer
    lexer.input(s)

def initf(fname):
    f = open(fname)
    global lexer
    lexer.input(f.read())
    f.close

# get a token

def token():
    global lexer
    return lexer.token()

# interprit the selected string

def lexs(s):
    global lexer
    lexer.input(s)

    acc = []
    while True:
        t = lexer.token()
        if verbose:
            print(t)
        if not t:
            break

        acc.append(t)

    return acc

# interprit the named file

def lexf(fname):
    f = open(fname)
    acc = lexs(f.read())
    f.close()
    return acc
