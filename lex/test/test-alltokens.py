#!/usr/bin/python

import mtylex as lex
import difflib

from colorama import init
init()
from colorama import Fore, Back, Style

# lex.verbose = True

out = str(lex.lexf("alltokens.mty"))
cf = open("test-alltokens.out")
crct = cf.read()

fail = False

for i, s in enumerate(difflib.ndiff(crct, out)):
    if s[0] == ' ':
        print(Fore.RESET + s[-1], end = '')
    elif s[0] == '+':
        print(Fore.YELLOW + s[-1], end = '')
        fail = True
    elif s[0] == '-':
        print(Fore.RED + s[-1], end = '')
        fail = True

if fail:
    print(Fore.WHITE + Back.RED + Style.BRIGHT)
    print("Test Failed")
else:
    print(Fore.BLACK + Back.GREEN + Style.BRIGHT)
    print("Test Succeeded")

print(Style.RESET_ALL)
