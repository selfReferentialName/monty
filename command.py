#!/usr/bin/python3

import argparse
from parser.parser import parse
from llvm.file import llvm

ap = argparse.ArgumentParser(description="Python based bootstrap compiler for Oxyl.")
ap.add_argument("infile", nargs=1, help="the file to compile")

args = ap.parse_args()

llvm(parse(args.infile[0] + ".xyl"), args.infile[0])
