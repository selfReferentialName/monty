#!/usr/bin/python

from lex import mtylex as lex
from lex.mtylex import tokens
from ast.word import word

fail = False

# precidence rules

precedence = (
    ('left', 'PR_FINAL'),
    ('left', 'PR_MIN'),
    ('left', 'PR_FILE'),
    ('right', 'PR_LINE'),
    ('right', 'PR_EQUALS'),
    ('right', 'PR_IDENT_EXP'),
    ('left', 'LOGIC'),
    ('left', 'CMP'),
    ('right', 'PR_FUNC'),
    ('left', 'ADDSUB'),
    ('left', 'MULDIV'),
    ('left', 'PR_FUNC_DECL_CONT'),
    ('left', 'PR_FUNC_DECL'),
    ('left', 'PR_IDENT_DECL'),
#    ('right', 'PR_DESCR'),
    ('left', 'PR_LIT_INT'),
    ('left', 'PR_MAX')
)

from ply import yacc as yacc

#
# syntax rules
#

def p_final(p):
    'final : file %prec PR_FINAL'
    p[0] = p[1]

# error convienience strings
import colors as c

def p_error(p):
    print('%sSyntax Error on line %s%d%s with token %s%s' % (c.rnbg, c.bonr, p.lineno, c.rnbg, p, c.reset))
    global fail
    fail = True
    if p:
        print(c.note + "Continuing on next line to look for more errors, there may be other errors in this line" + c.reset)
        tok = parser.token()
        while tok and tok.type != 'NEWLINE':
            tok = parser.token()
        parser.errok()
        return tok
    else:
        print(note + "Unnexpected EOF" + c.reset)

# import rules

from ast.imprt import imp

def p_import(p):
    'import : IMPORT IDENTIFIER %prec PR_MAX'
    p[0] = imp([word(p[2], word.Type.IDENTIFIER),])

def p_import_cont(p):
    'import : import DOT IDENTIFIER %prec PR_MAX'
    p[0] = p[1]
    p[0].path.append(word(p[3], word.Type.IDENTIFIER))

# declaration rules

from ast.declaration import desc, decl, fdecl

def p_described_init(p):
    '''described : INT_T
                 | FLOAT_T
                 | STRING_T
                 | VOID_T %prec PR_MIN'''
    p[0] = desc(word(p[1], word.Type.TYPE))

def p_described_array(p):
    'described : ARRAY_T LESS described GREATER %prec PR_MAX'
    p[0] = desc(word(p[1], word.Type.TYPE), p[3])

def p_described_vec(p):
    'described : VEC_T LESS described GREATER LESS INT GREATER %prec PR_MAX'
    p[0] = desc(word(p[1], word.Type.TYPE), p[3], p[6])

def p_described_mat(p):
    'described : MAT_T LESS described GREATER LESS INT INT GREATER %prec PR_MAX'
    p[0] = desc(word(p[1], word.Type.TYPE), p[3], p[6], p[7])

def p_described_cont(p):
    'described : DESC_D described %prec PR_MAX'
    p[0] = p[2]
    if isinstance(p[0].desc, list):
        p[0].desc.append(p[1])
    else:
        p[0].desc = [p[1],]

def p_declare(p):
    'declare : described IDENTIFIER %prec PR_IDENT_DECL'
    p[0] = decl(p[1], word(p[2], word.Type.IDENTIFIER))

def p_fdeclare_init(p):
    'fdeclare : declare declare %prec PR_FUNC_DECL'
    p[0] = fdecl(p[1].typ, p[1].ident, p[2])

def p_fdeclare_cont(p):
    'fdeclare : fdeclare declare %prec PR_FUNC_DECL_CONT'
    p[0] = p[1]
    p[0].args.append(p[2])

def p_fdeclare_void(p):
    'fdeclare : declare VOID_T %prec PR_FUNC_DECL'
    p[0] = fdecl(p[1].typ, p[1].ident)

# expression rules

from ast.expression import exp
from ast.operator import op
from ast.operator import opr

def p_expression_int(p):
    'expression : INT %prec PR_LIT_INT'
    p[0] = exp(exp.Type.LITERAL_INT, p[1])

def p_expression_string(p):
    'expression : STRING %prec PR_MAX'
    p[0] = exp(exp.Type.LITERAL_STRING, p[1])

def p_expression_float(p):
    'expression : FLOAT %prec PR_MAX'
    p[0] = exp(exp.Type.LITERAL_FLOAT, p[1])

def p_expression_ident(p):
    'expression : IDENTIFIER %prec PR_IDENT_EXP'
    p[0] = exp(exp.Type.IDENTIFIER, p[1])

def p_expression_op(p):
    '''expression : expression EQUALS expression %prec PR_EQUALS
                  | expression LOGIC expression
                  | expression ADDSUB expression
                  | expression MINUS expression %prec PR_MAX
                  | expression MULDIV expression'''
    p[0] = exp(opr(p[2]), p[1], p[3])

def p_expression_parens(p):
    'expression : PAREN_L expression PAREN_R %prec PR_MAX'
    p[0] = p[2]

def p_expression_call_start(p):
    'expression : IDENTIFIER expression %prec PR_FUNC'
    p[0] = exp(word(exp.Type.CALL, p[1], word.Type.IDENTIFIER), [p[2],], True)

def p_call_cont(p):
    'expression : expression expression %prec PR_FUNC'
    p[0] = p[1]
    if isinstance(p[0].b, list):
        p[0].b += [p[2],]
    else:
        p[0].b = [p[0].b, p[2]]

#def p_expression_call(p):
#    'expression : call %prec PR_MIN'
#    p[0] = p[1]

# block rules

from ast.function import condDef, line, cond, func

class Tabs():
    def __init__(self, val):
        self.v = val

def p_tabs_start(p):
    'tabs : TAB %prec PR_MAX'
    p[0] = Tabs(1)

def p_tabs_cont(p):
    'tabs : tabs tabs %prec PR_MAX'
    p[0] = Tabs(p[1].v + p[2].v)

# for a line in a block
def p_line_exp(p):
    'line : tabs expression NEWLINE %prec PR_LINE'
    p[0] = line(p[1].v, p[2])

def p_line_eq_block(p):
    'line : tabs declare EQUALS expression NEWLINE %prec PR_EQUALS'
    p[0] = line(p[1].v, p[4], p[2])
    
def p_func_start(p):
    'func : fdeclare NEWLINE line %prec PR_MAX'
    p[0] = func(p[1], [p[3],])

def p_func_cont(p):
    'func : func line %prec PR_MAX'
    p[0] = p[1]
    p[0].lines.append(p[2])

# file rules

from ast.file import file_s as fil

def p_defn(p):
    'defn : declare EQUALS expression NEWLINE %prec PR_LINE'
    p[0] = line(0, p[3], p[1])

def p_file_start(p):
    '''file : func %prec PR_FILE
            | import NEWLINE %prec PR_FILE
            | defn %prec PR_FILE'''
    p[0] = fil([p[1],])

def p_file_cont(p):
    'file : file file %prec PR_FILE'
    p[0] = p[1]
    p[0].blocks += p[2].blocks

def p_file_absorb(p):
    'file : NEWLINE file %prec PR_MIN'
    p[0] = p[2]

# run the parser

parser = yacc.yacc(debug = True)

def parse():
    global ast, parser
    ast = parser.parse(lexer=lex)
    return ast

def parses(s):
    lex.inits(s)
    global ast, parser
    ast = parser.parse(lexer=lex)
    return ast

def parse(f):
    lex.initf(f)
    global ast, parser
    ast = parser.parse(lexer=lex)
    print(ast)
    return ast
