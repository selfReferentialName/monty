// Library initialization

#include "string.h"
#include "array.h"
#include <string.h>
#include <stdlib.h>

void prog (struct ox_array*);

int main (int argc, char **argv) {
	struct ox_array args;
	args.bound = argc;
	size_t n = sizeof(struct ox_string);
	struct ox_string* data = malloc(n * argc);
	for (int i = 0; i < argc; i++) {
		data[i].bound = strlen(argv[i]);
		data[i].s = argv[i];
	}
	args.s = data;
	prog(&args);
	return 0;
}
