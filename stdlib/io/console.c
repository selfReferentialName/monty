// io.console implementation

#include "string.h"
#include <stdio.h>

void print(struct ox_string* s) {
	printf("%s\n", s->s);
}

void put(struct ox_string* s) {
	printf("%s", s->s);
}
