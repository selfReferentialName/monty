#!/usr/bin/py

# all types of words (keywords and words)

from enum import Enum, unique
import sys

class word:

    @unique
    class Type(Enum):
        DESCRIPTOR = 0
        TYPE = 1
        CONTROL = 2
        IMPORT = 3
        IDENTIFIER = 4
        ERROR = 5

    def __init__(self):
        self.t = ERROR
        self.v = ""

# DO NOT USE
    def __init__(self, tok):
        print(tok)
        if tok.type.endswith('_D'):
            self.t = Type.DESCRIPTOR
        elif tok.type.endswith('_T'):
            self.t = Type.TYPE
        elif tok.type.endswith('_C'):
            self.t = Type.TYPE
        elif tok.type == 'IMPORT':
            self.t = Type.IMPORT
        elif tok.type == 'IDENTIFIER':
            self.t = Type.IDENTIFIER
        else:
            self.t = Type.ERROR
            print("Invalid token passed to AST: " + tok)
            sys.exit("AST invariant broken")
        self.v = tok.value

    def __str__(self):
        return '%s(%s)' % (self.t, self.v)

    def __init__(self, tok, typ):
        self.v = str(tok)
        self.t = typ

    def __repr__(self):
        return self.v
