#!/usr/bin/python

class imp:
    def __init__(self):
        self.path = []

    def __init__(self, p = []):
        self.path = p

    def __str__(self):
        path = ""
        for p in self.path:
            path += str(p) + "."
        return 'import(%s)' % path.strip(".")

    # get the path name
    def pname(self):
        path = ""
        for p in self.path:
            path += p.v + "/"
        return path.rstrip("/") + ".imf"
