#!/usr/bin/py

# all types of literals

from enum import Enum, unique

class lit:

    @unique
    class Type(Enum):
        ERROR = 0
        INT = 1
        STRING = 2

    def __init__(self):
        self.t = Type.ERROR
        self.v = None
