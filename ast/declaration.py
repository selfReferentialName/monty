#!/usr/bin/python

from ast.word import word
from enum import Enum, unique

class desc:
    def __init__(self):
        self.desc = word()
        self.typ = None
        self.cont = None

    def __init__(self, tp):
        self.desc = None
        self.typ = tp
        self.cont = None

    def __init__(self, tp, c = None, n = 1, m = 1):
        self.desc = None
        self.typ = tp
        self.cont = c
        self.n = n
        self.m = m

    def __str__(self):
        return 'desc(%s %s<%s>)' % (self.desc, self.typ, self.cont)

    def __repr__(self):
        return '%r-%r' % (self.typ, self.cont)

class decl:
    def __init__(self):
        self.typ = desc()
        self.ident = word()

    def __init__(self, tp, idt):
        self.typ = tp
        self.ident = idt

    def __str__(self):
        return 'decl(%s %s)' % (self.typ, self.ident)

class fdecl():
    def __init__(self):
        self.args = [] # a list of decl

    def __init__(self, tp, idt, arg = None): 
        self.typ = tp
        self.ident = idt
        self.args = [arg,]

#    def __init__(self, dec, arg):
#        self.typ = dec.typ
#        self.ident = dec.ident
#        self.args = [arg,]

#    def __init__(self, dec):
#        self.typ = dec.typ
#        self.ident = dec.ident
#        self.args = []

    def __str__(self):
        args = ""
        for arg in self.args:
            args += str(arg) + ","
        return 'fdecl(%s %s %s)' % (self.typ, self.ident, args.strip(","))
