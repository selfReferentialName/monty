#!/usr/bin/python

# function blocks and stuff

from ast.expression import exp
import ast.declaration as d
from ast.word import word

class condDef:
    def __init__(self):
        self.cond = word()
        self.ex = exp()

    def __str__(self):
        return 'cd-%s(%s)' % (self.cond, self.ex)

class line:
    def __init__(self):
        self.ex = exp()
        self.inden = 0
        self.decl = None

    def __init__(self, i, expr):
        self.ex = expr
        self.inden = i
        self.decl = None

    def __init__(self, i, expr, dc = None):
        self.ex = expr
        self.iden = i
        self.decl = dc

    def __str__(self):
        if self.decl == None:
            return 'line(%d %s)' % (self.iden, self.ex)
        else:
            return 'line(%d %s %s)' % (self.iden, self.decl, self.ex)

class cond:
    def __init__(self):
        self.cond = condDef
        self.lines = []

    def __str__(self):
        return 'cond(%s %s)' % (self.cond, self.lines)

class func:
    def __init__(self):
        self.func = d.fdecl()
        self.lines = [] # also includes condition blocks

    def __init__(self, fdecl, lins):
        self.func = fdecl
        self.lines = lins

    def __str__(self):
        lines = ""
        for line in self.lines:
            lines += "\n\t" + str(line)
        return 'func(%s %s)' % (self.func, lines)
