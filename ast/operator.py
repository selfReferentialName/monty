#!/usr/bin/python

from enum import Enum, unique

import sys

@unique
class op(Enum):
    EQUALS = 0
    LESS = 1
    GREATER = 2
    PLUS = 3
    COMMA = 4
    MINUS = 6
    TIMES = 7
    DIV = 8
    REM = 9
    AND = 10
    OR = 11
    XOR = 12
    NOT = 13
    COLON = 15
    DOT = 16

def opr(tok):
    if tok == '=':
        return op.EQUALS
    elif tok == '<':
        return op.LESS
    elif tok == '>':
        return op.GREATER
    elif tok == '+':
        return op.PLUS
    elif tok == ',':
        return op.COMMA
    elif tok == '-':
        return op.MINUS
    elif tok == '*':
        return op.TIMES
    elif tok == '/':
        return op.DIV
    elif tok == '%':
        return op.REM
    elif tok == '&':
        return op.AND
    elif tok == '|':
        return op.OR
    elif tok == '^':
        return op.XOR
    elif tok == '!':
        return op.NOT
    elif tok == ':':
        return op.COLON
    elif tok == '.':
        return op.DOT
    else:
        print("Invariant broken: unrecognized operator " + tok)
        sys.exit("Compiler is broken")
