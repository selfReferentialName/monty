#!/usr/bin/python

class file_s:
    def __init__(self):
        self.blocks = [] # function blocks, definitions, and import statements

    def __init__(self, b):
        self.blocks = b

    def __str__(self):
        acc = ""
        for block in self.blocks:
            acc += str(block) + "\n"
        return acc
