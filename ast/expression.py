#!/usr/bin/python

from enum import Enum, unique

from ast.operator import op

class exp:

    @unique
    class Type(Enum):
        LITERAL_INT = 0
        IDENTIFIER = 1
        CALL = 2
        CONTAINER_ARRAY = 3
        LITERAL_STRING = 4
        ERROR = 5
        OPERATION = 6
        DEFINITION = 7
        LITERAL_FLOAT = 8

    @unique
    class Result(Enum):
        VOID = 0
        INT = 1
        ARRAY = 2 # also works for strings
        IND = 3 # cannot be deduced in the AST, update in the backend, set dirty = True and call update() on the most recent non-call expression
        VEC = 4
        FLOAT = 5

    def __init__(self):
        self.o = op()
        self.a = None
        self.b = None
        self.t = Type.ERROR

    def __init__(self, ty, val):
        self.t = ty
        self.a = val
        self.b = None

    def __init__(self, opr, aa, bb = None, to = False):
        global exp
        if bb == None or to:
            self.t = opr
        else:
            self.t = exp.Type.OPERATION
            self.o = opr
        self.a = aa
        self.b = bb
        self.r = exp.Result.IND
        self.dirty = True
        self.rt = None

    def func(self, func, args):
        global exp
        self.t = exp.Type.CALL
        self.a = func
        self.b = args

    def defn(self, dec, ex):
        global exp
        self.t = exp.Type.DEFINITION
        self.a = dec
        self.b = ex

    def fix(self):
        global exp
        if isinstance(self.b, list):
            self.t = exp.Type.CALL
            if isinstance(self.a, exp):
                self.a = self.a.a
            acc = []
            for b in self.b:
                if b != None:
                    acc.append(b)
                    if isinstance(b, exp):
                        b.fix()
            self.b = acc
        elif isinstance(self.b, exp):
            self.b.fix()
            self.r = self.b.r # this will get overridden by setting it later
        if isinstance(self.a, exp):
            self.a.fix()
            self.r = self.a.r
        if self.t == exp.Type.LITERAL_INT:
            self.r = exp.Result.INT
        elif self.t == exp.Type.LITERAL_STRING:
            self.r = exp.Result.ARRAY
        self.dirty = False

    def update(self):
        global exp
        if self.dirty:
            self.dirty = False
            return
        if isinstance(self.b, exp):
            self.b.update()
            self.r = self.b.r
        if isinstance(self.a, exp):
            self.a.update()
            self.r = self.a.r

    def __str__(self):
        global exp
        self.fix()
        if self.t == exp.Type.ERROR:
            return 'exp-ERROR'
        elif self.t == exp.Type.OPERATION:
            return 'exp-op(%s %s %s)' % (self.a, self.o, self.b)
        elif self.t == exp.Type.CALL or self.t == exp.Type.DEFINITION:
            b = ""
            for bb in self.b:
                b += str(bb) + ","
            return 'exp-%s(%s %s)' % (self.t, self.a, b.strip())
        else:
            return 'exp-%s(%s)' % (self.t, self.a)
