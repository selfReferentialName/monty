# handles outputing the declaration and defenitions of functions

from . import typ
from .expression import expr
from .liner import Liner

# a list of functions indexed by names producing how to call
funcs = {}
# the argument lists
args = {}
# a set of all void functions
voids = set()
# all types of all functions not in voids
types = {}

def arglist(fdecl):
    args[fdecl.ident.v] = ()
    acc = ""
    for a in fdecl.args:
        if a != None and typ.name(a) != None:
            args[fdecl.ident.v] += typ.name(a),
            acc += typ.dec(a) + ", "
    return acc.strip(", ")

def header(fdecl):
    linkage = "internal"
    cconv = "fastcc"
    if isinstance(fdecl.typ.typ, list):
        for d in fdecl.typ.typ:
            if d == "global":
                linkage = "externel"
            elif d == "public":
                print(fdecl, "is public")
                linkage = "external"
                cconv = "ccc"
            elif d == "cold":
                cconv = "cold"
            elif d == "common":
                linkage = "linkonce_odr"
    funcs[fdecl.ident.v] = '%s %s @%s' % (cconv, typ.name(fdecl), fdecl.ident.v)
    t = typ.name(fdecl)
    if t == "void" or t == None:
        voids.add(fdecl.ident.v)
        t = "void"
    else:
        types[fdecl.ident.v] = t
    return '%s %s %s @%s (%s) %s' % (linkage, cconv, t, fdecl.ident.v, arglist(fdecl), attrs(fdecl))

def attrs(fdecl):
    acc = ""
    willreturn = True
    if isinstance(fdecl.typ.typ, list):
        for d in fdecl.typ.typ:
            if d == "cold":
                acc += "cold "
            elif d == "global":
                acc += "norecurse "
            elif d == "common":
                acc += "norecurse "
            elif d == "public":
                willreturn = False
    if willreturn:
        acc += "#0"
    return acc

def declaration(fdecl):
    return "declare " + header(fdecl) + arglist(fdecl)

def line(l, liner, locs):
    return expr(l.ex, liner, locs)

def definition(func):
    liner = Liner()
    acc = "define " + header(func.func) + arglist(func.func) + "{"
    for l in func.lines:
        acc += "\t" + line(l, liner, {})[1] + "\n"
    if func.func.ident.v in voids:
        acc += "\tret void\n"
    acc += "}"
    return acc
