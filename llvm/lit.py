# handles literals

from ast.expression import exp
import re
import struct
from . import typ
from .types import string

consts = "" # emit this at the top of the output file
globs = 0 # index to use to name globals
printable = re.compile(r'[-A-Za$._0-9]')

# escape a string in LLVM style
def escape(st):
    s = ""
    for c in st:
        if re.match(printable, c) == None:
            s += r'\%02x' % ord(c)
        else:
            s += c
    return s

# returns the string for the representation or None if not a literal
def rep(ex):
    if ex.t == exp.Type.LITERAL_INT:
        ex.rt = "i32"
        ex.dirty = True
        return "%d" % ex.a
    elif ex.t == exp.Type.LITERAL_STRING:
        global consts, globs
        typ.use(string)
        consts += '@lit-%d.str = private constant [%d x i8] c\"%s\\00\"\n' % (globs, len(ex.a) + 1, ex.a)
        i = str(globs)
        vs = str(len(ex.a))
        ps = str(len(ex.a) + 1)
#       ty = "{i32, [" + ps + " x i8]*} "
#       consts += '@lit-' + i + " = private constant " + ty + "{i32 " + vs + ", [" + ps + " x i8]* @lit-" + i + ".str}\n"
#       consts += '@lit-{0:d} = private constant \\{i32 {1:d}, [{2:d} x i8]* @lit-{0:d}.str\\}\n'.format(globs, len(ex.a), len(ex.a) + 1)
        consts += '@lit-' + i + " = private constant %tp-string {i32 " + vs + ", i8* getelementptr inbounds([" + ps + " x i8], [" + ps + " x i8]* @lit-" + i + ".str, i32 0, i32 0)}\n"
        globs += 1
        name = '@lit-%d' % (globs - 1,)
#       return 'getelementptr inbounds (%%tp-string, %%tp-string* %s, i32 0)' % name
        ex.rt = "%tp-string"
        ex.dirty = True
        return name
    elif ex.t == exp.Type.LITERAL_FLOAT:
        # this one is much more interesting
        # we need to make sure it fits exactly
        ex.rt = "double"
        ex.dirty = True
        return "0x" + ("%016x" % ex.a)[0:15]
    else:
        return None
