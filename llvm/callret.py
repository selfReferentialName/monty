# handles calls, returns, and all that jazz

def ret(t, v):
    if t == "void":
        return "ret void"
    else:
        return "ret %s %s" % (t, v)

# end of function
def fin(t):
    if t == "void":
        return "ret void"
    else:
        return "unreachable"

def call(n, args, vals, r = None):
    print(args, vals)
    acc = "("
    if len(vals) != 0:
        for arg, val in zip(args, vals):
            print(arg, val)
            acc += arg + " " + val + ", "
    acc = acc.strip(", ") + ")"
    if r == None:
        return "call %s %s" % (n, acc)
    else:
        return "%s = call %s %s" % (r, n, args)
