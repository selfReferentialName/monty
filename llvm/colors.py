# nice colors for printing in a module

from colorama import init
init()
from colorama import Fore, Back, Style

rnbg = Fore.RED + Back.RESET
bonr = Fore.BLACK + Back.RED
note = Fore.YELLOW + Back.RESET + Style.DIM
reset = Style.RESET_ALL
