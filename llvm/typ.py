# interface with the types package in a way that is nice for use in other places

from .types import intt, string, array, void

# all types that have been used before
used = set()

# output specifications
specs = ""

types = (
    ("int", intt),
    ("string", string),
    ("array", array),
    ("void", void)
)

# requires the module to accept no arguments
def use(m):
    if m.name() not in used:
        global specs
        used.add(m.name())
        specs += m.spec() + "\n"

def name(decl):
    for t, m in types:
        if decl != None and decl.typ != None and t != None and decl.typ.typ == t:
            if m.name(decl) not in used:
                global specs
                used.add(m.name(decl))
                specs += m.spec(decl) + "\n"
            return m.name(decl)

# in local scope
def dec(decl):
    return '%s @%s' % (name(decl), decl.ident.v)

def rtot(a = None):
    raise Exception("TODO")

def ttor(t):
    return t
