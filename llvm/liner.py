# the module of the Liner class
# Local
# Identifier
# Number
# Expression
# Result
# the Liner class tracks numbers to be used as identifiers in SSA form

class Liner:
    def __init__(self):
        self.i = -1

    # get a new identifier number
    def pop(self):
        self.i += 1
        return self.i

    # has an identifier number been used
    def used(self, i):
        if i < self.i and i >= 0:
            return True
        else:
            return False
