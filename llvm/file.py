# Kronk: Oh yeah, it's all coming together.
# this file output codes for the entire file and runs everyting except the lexer/parse

import os
from .function import definition
from .imp import read
from ast.imprt import imp
from ast.function import func
from . import lit
from . import typ

def llvm(ast, name):
    d = os.path.dirname(os.path.abspath(__file__)) + "/"
    f = open(name + ".ll", 'w')
    f.write(open(d + "header.ll").read())
    body = ""
    for b in ast.blocks:
        if isinstance(b, imp):
            body += read(b.pname())
        elif isinstance(b, func):
            body += definition(b)
    f.write(typ.specs)
    f.write(lit.consts)
    f.write(body)
    f.close()
    os.system(d + "passes.sh " + name)
