#!/bin/sh

# run transformation passes on the code for optimization
# it must be fed a $1 that has no ending and can be appended with .ll for a valid llvm assembly file

bc="$1.bc"

opt -mem2reg -tailcallelim -constprop -die -functionattrs "$1.ll" -o $bc
