# string type

def spec(ignore = None):
    return r'%tp-string = type { i32, i8* }'

def name(ignore = None):
    return r'%tp-string'
