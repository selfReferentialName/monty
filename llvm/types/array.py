# the array type

def spec(desc):
    return r'@tp-%r = type { i32, @%r* }' % (desc, desc.cont)

def name(desc):
    return r'@tp-%r' % desc
