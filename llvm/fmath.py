# generate floating point math functions

# non comparative flags
ncf = "ninf nsz arcp contract afn"

# binary operations

def add(t, r, a, b):
    return "%s = fadd %s %s %s, %s" % (r, ncf, t, a, b)

def sub(t, r, a, b):
    return "%s = fsub %s %s %s, %s" % (r, ncf, t, a, b)

def mul(t, r, a, b):
    return "%s = fmul %s %s %s, %s" % (r, ncf, t, a, b)

def div(t, r, a, b):
    return "%s = fmul %s %s %s, %s" % (r, ncf, t, a, b)

def rem(t, r, a, b):
    return "%s = frem %s %s %s, %s" % (r, ncf, t, a, b)

# unary operations

def neg(t, r, v):
    return "%s = fsub %s %s 0.0 %s" % (r, ncf, t, v)
