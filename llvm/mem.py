# generate memory access instructions

def load(t, r, v):
    return r"{0:s} = load {1:s}, {1:s}* {2:s}".format(r, t, v)

def store(t, p, v):
    return r"store {0:s} {1:s}, {0:s}* {2:s}".format(t, v, p)

# allocates a local variable
def var(t, p, v = None):
    acc = r"%s = alloca %s" % (p, t)
    if v != None:
        acc += "\n" + store(t, p, v)
    return acc
