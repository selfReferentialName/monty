# generate math of integers
# all names are finished llvm identifers

# binary operators (all primitive LLVM operations are binary

def add(t, r, a, b):
    return "%s = add nuw nsw %s %s, %s" % (r, t, a, b)

def sub(t, r, a, b):
    return "%s = sub nuw nsw %s %s, %s" % (r, t, a, b)

def mul(t, r, a, b):
    return "%s = mul nuw nsw %s %s, %s" % (r, t, a, b)

# currently, all ints are signed
def div(t, r, a, b):
    return "%s = sdiv %s %s, %s" % (r, t, a, b)

# the result shares the same sign as a
def rem(t, r, a, b):
    return "%s = srem %s %s, %s" % (r, t, a, b)

# currently requires that b is less than the size of the type
def shl(t, r, a, b):
    return "%s = shl nsw %s %s, %s" % (r, t, a, b)

# arythmatic since all ints are signed
def shr(t, r, a, b):
    return "%s = ashr %s %s, %s" % (r, t, a, b)

def andd(t, r, a, b):
    return "%s = and %s %s, %s" % (r, t, a, b)

def orr(t, r, a, b):
    return "%s = or %s %s, %s" % (r, t, a, b)

def xorr(t, r, a, b):
    return "%s = xor %s %s, %s" % (r, t, a, b)

# unary operations (implemented using binary operations)

def neg(t, r, i):
    return sub(t, r, "%d" % 0, i)
