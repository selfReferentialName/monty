# expression code generator

from . import typ
from ast.expression import exp
from ast.operator import op
from .mem import load, store
from .typ import rtot, ttor
#from .function import args, funcs, voids, types
from . import function as funct
from .callret import call, ret
from . import lit as lits # typo
from . import colors as c
from . import imath
from . import fmath

class ExpressionError(Exception):
    def __init__(self, msg, ex):
        self.msg = msg
        self.ex = ex

    def __str__(self):
        return "%s%s: in expression %s%s" % (self.msg, c.note, self.ex, c.reset)

# returns (the name of the result, code generated)
# ex is the expression
# liner is the LINER
# locs is a dict of all local identifiers storing type names
def expr(ex, liner, locs):
    lr = lits.rep(ex)
    if lr != None:
        return (lr, "")
    elif ex.t == exp.Type.IDENTIFIER:
        r = "%%%d" % liner.pop()
        if ex.a in locs:
            name = "%%%s" % ex.a
        else:
            name = "@%s" % ex.a
        return (r, "\t" + load(locs[ex.a], name, r))
    elif ex.t == exp.Type.CALL:
        vals = ()
        code = ""
        for arg in ex.b:
            if not isinstance(arg, exp):
                continue
            vc = expr(arg, liner, locs)
            vals += (vc[0],)
            code += "\t" + vc[1] + "\n"
        if ex.a in funct.voids:
            ex.r = exp.Result.VOID
            ex.dirty = True
            return (None, call(funct.funcs[ex.a], funct.args[ex.a], vals))
        else:
            ex.rt = funct.types[ex.a]
            if ex.rt.startswith("i"):
                ex.r = exp.Result.INT
            elif ex.rt == "double" or ex.rt == "float" or ex.rt == "half" or ex.rt == "fp128" or ex.rt == "x86_fp80" or ex.rt == "ppc_fp128":
                ex.r = exp.Result.FLOAT
            ex.dirty = True
            return (ex.r, call(funct.funcs[ex.a], funct.args[ex.a], vals))
    elif ex.t == exp.Type.ERROR:
        raise ExpressionError(c.bonr + "Some strange internal error led to an invalid expression", ex)
    if ex.t == exp.Type.OPERATION:
        # pass 1 to get rt
        ex.oplla = expr(ex.a, liner, locs)
        ex.opllb = expr(ex.b, liner, locs)
    if ex.r == exp.Result.IND:
        ex.update()
        if ex.r == exp.Result.IND:
            raise ExpressionError(c.bonr + "Cannot determine type of expression", ex)
    # now for all different types of operations
    if ex.t == exp.Type.OPERATION:
        # pass 2 to finish
        r = "%%%d" % liner.pop()
        # select a module to do the math
        m = None
        if ex.r == exp.Result.INT:
            m = imath
        elif ex.r == exp.Result.FLOAT:
            m = fmath
        acc = ex.oplla[1] + "\n" + ex.oplla[1]
        # select the math to do
        if ex.o == op.PLUS:
            return (r, acc + m.add(ex.rt, r, ex.oplla[0], ex.opllb[0]))
        elif ex.o == op.MINUS:
            return (r, acc + m.sub(ex.rt, r, ex.oplla[0], ex.opllb[0]))
        elif ex.o == op.TIMES:
            return (r, acc + m.mul(ex.rt, r, ex.oplla[0], ex.opllb[0]))
        elif ex.o == op.DIV:
            return (r, acc + m.div(ex.rt, r, ex.oplla[0], ex.opllb[0]))
        elif ex.o == op.REM:
            return (r, acc + m.rem(ex.rt, r, ex.oplla[0], ex.opllb[0]))
        elif ex.o == op.AND:
            return (r, acc + m.andd(ex.rt, r, ex.oplla[0], ex.opllb[0]))
        elif ex.o == op.OR:
            return (r, acc + m.orr(ex.rt, r, ex.oplla[0], ex.opllb[0]))
        elif ex.o == op.XOR:
            return (r, acc + m.xorr(ex.rt, r, ex.oplla[0], ex.opllb[0]))
        raise ExpressionError(c.rnbg + "invalid operation", ex)
    raise ExpressionError(c.rnbg + "I haven't programmed that path yet.", ex)
