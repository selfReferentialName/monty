# read and write IMF
# also handles import statements

import csv
from . import function

path = ["", "~/.share/oxyl/include/", "/usr/share/oxyl/include/"]
csv.register_dialect("imf", "unix", delimiter='+', doublequote=False, escapechar='\\')

def rdline(line, globs):
    if line[0] == "version":
        assert line[1] == "1.0.0", "Unsupported version number"
    elif line[0] == "var" or line[0] == "const":
        globs += line[1] + "\n"
    elif line[0] == "func":
        globs += "declare " + line[1] + "\n"
        function.funcs[line[2]] = line[3]
        function.args[line[2]] = line[4:]
        function.voids.add(line[2])
    elif line[0] == "global":
        globs += line[1] + "\n"
    else:
        raise Exception("IMF file not supported")
    return globs

def read(fname):
    globs = ""
    for p in path:
        print(p)
        print(fname)
        global f
        try:
            f = open(str(p) + str(fname), newline = '')
        except FileNotFoundError:
            continue
        break
    c = csv.reader(f, "imf")
    for line in c:
        globs = rdline(line, globs)
    f.close()
    print(globs)
    return globs
