; header file for llvm output

target datalayout = "e-S128-p:64:64-m:e-n8:16:32:64-a:0:128"
target triple = "x86_64-pc-linux-gnu"

declare external ccc i8* @malloc (i64)

define linkonce fastcc i8* @rt-malloc (i32 %foo) {
	%1 = zext i32 %foo to i64
	%2 = tail call ccc i8* @malloc(i64 %1)
	ret i8* %2
}

!0 = !{ i32 1, !"wchar_size", i32 4 }
!llvm.module.flags = !{ !0 }

!1 = !{!"Oxyl Python compiler version 0.1.0 (64-bit malloc) for LLVM 6.0.1 through 10"}
!llvm.ident = !{ !1 }

; attributes #0 = { willreturn nounwind } ; guarentees for non-public functions
%tp-string = type { i32, i8* }
@lit-0.str = private constant [12 x i8] c"Hello World\00"
@lit-0 = private constant %tp-string {i32 11, i8* getelementptr inbounds([12 x i8], [12 x i8]* @lit-0.str, i32 0, i32 0)}
declare void @print(%tp-string*) noinline nounwind optnone uwtable
define internal fastcc void @prog () #0{	call void @print (%tp-string* @lit-0)
	ret void
}