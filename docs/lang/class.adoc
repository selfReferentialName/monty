Classses in Oxyl
----------------

While oxyl is first and foremost a procedural language, it also provides simple
object programming interface (without run time types) in order to provide a good
method for dealing with things like objects (C isn't that good with its struct
system). It is important to note that not all objects have classes (all user
defined and library classes are though). Objects with a class are _*classy*_,
and those without a class are _*classless*_.

A Note on Class Definitions
---------------------------

All stuff about a class is put in the class definition, even the definitions of
all its functions (the class itself fas functions, therefore only classy objects
have methods).

Class Headers
-------------

Class definitions start with the following: +
+class Foo+ +
and is followed by indented lines in the same way as a normal file.

To make a class that extends another class (or type), use +
+class Foo Bar+

Variables in all Classes
------------------------

Every classy object has certain member variables.

 * +self+
 ** The +self+ variable refers to the object being called.

Constructors
------------

Classes have constructors. The purpose of a constructor is not to allocate the
memory for the object (that is done automatically by +new+ and +use+), but does
initialize all children and initial values. It is required to set all variables
to some value, but this can just be +null+. Every class must have at least one
constructor.

Constructors are declared using the following syntax (assuming the class is
named Foo): +
+Foo void+ for a default constructor, and +
+Foo string msg+ for a constructor that takes a single string argument.

Converters
----------

Implicit conversion is done through the use of converters. To force the invoking
of a converter, use +(Foo bar)+, but because of strong typing, this is only
necessary to make conversion happen earlier. By default, implicit conversion is
done last, right before storing a variable or passing an argument.

From a class' perspective, there are two types of converters: _*constructing
converters*_ and _*exporting converters*_. Constructing converters take some
other object and make it into a object of the correct class. By default,
constructing converters make a +new+ object, but my invoking it with explicit
conversion of the form +(Foo local bar)+, it can make a +local+ object.
Constructing converters are effectively just constructors, but can be called
implicitly. Exporting constructors take an object of the class and make it into
another kind of object, returning that object. Exporting converters are just
like more normal functions of a class, and therefore are less flexible. The main
usage of them is to produce classless or opaque objects (as can be found as
builtins and in libraries).

To make a converter, simply create a function using the syntax +
+InType:OutType+

For example, a constructing converter in class +Foo+ from type +Bar+, declare +
+Bar:Foo+ +
and to export convert +Foo+ to a +string+, declare +
+Foo:string+

Converters are passed the variable +from+ to show the object being converted
from. In an exporting converter, +from+ is a synonym for +self+.

Destructors
-----------

When the reference count of a heap object (one made by +new+) reaches zero or a
+local+ object's calling function exits, a destructor is called and the memory
is released. The purpose of the destructor is to decrement the reference count
of all of its member (classy object) variables.

The syntax to declare a destructor is the following: +
+:void+ or +Foo:void+. +
It is designed to look like an exporting converter to type void, but it need not
return anything.
